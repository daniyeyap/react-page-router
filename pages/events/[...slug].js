import { useRouter } from "next/router";
import { getFilteredEvents } from "../../data";
import EventList from "../../components/events/EventList";
import ResultsTitle from "../../components/events/results-title";
import Button from "../../components/ui/button";
import ErrorAlert from "../../components/ui/error-alert";

export default function EventSlug(params) {
  const router = useRouter();
  const filterDate = router.query.slug;

  if (!filterDate) {
    return <p className="center">Loading.....</p>;
  }

  const Year = filterDate[0];
  const month = filterDate[1];

  const numYear = +filterDate[0];
  const numMonth = +filterDate[1];

  if (
    isNaN(numYear) ||
    isNaN(numMonth) ||
    numYear > 2030 ||
    numYear < 2021 ||
    numMonth < 1 ||
    numMonth > 12
  ) {
    return (
      <ErrorAlert>
        <p>No Event was found</p>
      </ErrorAlert>
    );
  }

  const filterEvents = getFilteredEvents({ year: numYear, month: numMonth });

  if (!filterEvents || filterEvents.length === 0) {
    return (
      <>
        <ErrorAlert>
          <p>No Event was found for this filter</p>
        </ErrorAlert>
        <div className="center">
          <Button link="/events">Show All Event</Button>
        </div>
      </>
    );
  }

  const date = new Date(numYear, numMonth - 1);
  return (
    <>
      <ResultsTitle date={date} />
      <EventList items={filterEvents} />
    </>
  );
}
