import { getAllEvents } from "../../data";
import EventList from "../../components/events/EventList";
import EventSearch from "../../components/events/EventSearch";
import { useRouter } from "next/router";

export default function EventPage(params) {
  const featureEvents = getAllEvents();
  const router = useRouter();

  const findEventHandle = (year, month) => {
    const fullpath = `/events/${year}/${month}`;
    router.push(fullpath);
  };

  return (
    <div>
      <EventSearch onSearch={findEventHandle} />
      <EventList items={featureEvents} />
    </div>
  );
}
