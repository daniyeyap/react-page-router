import { useRouter } from "next/router";
import { getEventById } from "../../data";
import EventSumaty from "../../components/event-detail/event-summary";
import EventLogistics from "../../components/event-detail/event-logistics";
import EventContent from "../../components/event-detail/event-content";

export default function EventDetails(params) {
  const router = useRouter();
  const event = getEventById(router.query.eventid);

  if (!event) {
    return <p>No event found</p>;
  }

  return (
    <>
      <EventSumaty title={event.title} />
      <EventLogistics
        date={event.date}
        address={event.location}
        image={event.image}
        imageAlt={event.title}
      />
      <EventContent>
        <p>{event.description}</p>
      </EventContent>
    </>
  );
}
