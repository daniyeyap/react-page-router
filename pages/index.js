import EventList from "../components/events/EventList";
import { getAllEvents, getFeaturedEvents } from "../data";

export default function HomePage(params) {
  const featureEvents = getFeaturedEvents();

  return (
    <div>
      <ul>
        <EventList items={featureEvents} />
      </ul>
    </div>
  );
}
