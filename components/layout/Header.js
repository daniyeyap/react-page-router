import Link from "next/link";
import classes from '../../styles/main-header.module.css'
export default function Header(params) {
    return <header className={classes.header}>
        <div className={classes.logo}>
            <Link href="/">Next Event</Link>
        </div>
        <nav className={classes.navigation}>
            <ul>
                <li>
                    <Link href="/events">Browse all event</Link>
                </li>
            </ul>
        </nav>
    </header>
};

